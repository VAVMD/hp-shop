var mysql      = require('mysql');
var connection = mysql.createPool({
    connectionLimit : 10,
    host     : '127.0.0.1',
    user     : 'root',
    password : '',
    database : 'first'
});

module.exports = connection;